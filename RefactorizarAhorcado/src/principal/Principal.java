package principal;

import java.io.File;
import java.util.Scanner;
/** 
 * JUEGO DEL AHORCADO
 * 
 * @author NEREA ORTIZ PE�ALOSA
 * @since 21-2-2018
 * @version 1.3
 */
public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	private static char[][] caracteresPalabra = new char[2][];
	private static String palabraSecreta;
	
	public static void main(String[] args) {

		
		rellenarVectorPalabras();
		Scanner input = new Scanner(System.in);
		
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		String caracteresElegidos = "";
		int fallos = 0;
		boolean acertado;
		System.out.println("Acierta la palabra");
		
		do {

			palabraOculta();

			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();

			fallos = contabilizarFallos(caracteresElegidos, fallos);

			dibujarAhorcado(fallos);
			
			acertado = finalDelJuego(fallos);

		} while (!acertado && fallos < FALLOS);

		input.close();
	}
	
	/**
	 * ESTE METODO PARA EL PROGRAMA SI BIEN EL USUARIO HA 
	 * PERDIDO O SI HA GANADO LA PARTIDA
	 * 
	 * @param fallos : FALLOS DEL USUARIO
	 * @return BOOLEAN PARA SABER SI SE HA ACERTADO LA PALABRA O NO
	 */
	private static boolean finalDelJuego(int fallos) {
		boolean acertado;
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
		acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		return acertado;
	}
	
	/**
	 * ESTE METODO VA DIBUJANDO EL AHORCADO SEG�N
	 * LOS FALLOS QUE VA TENIENDO EL USUARIO
	 * 
	 * @param fallos : FALLOS DEL USUARIO
	 */
	private static void dibujarAhorcado(int fallos) {
		switch (fallos) {
		case 1:
			case1();
			break;
		case 2:
			case2();
			break;
		case 3:
			case3();
			break;
		case 4:
			case4();
			break;
		case 5:
			case5();
			break;
		case 6:
			case6();;
			break;
		case 7:
			case7();
			break;
		}
	}
	
	/**
	 * ESTE METODO VA CONTANDO LOS FALLOS DEL USUARIO.
	 * 
	 * @param caracteresElegidos : CARACTERES ELEGIDOS POR EL USUARIO
	 * @param fallos : FALLOS DEL USUARIO
	 * @return DEVUELVE EL N�MERO DE FALLOS QUE TIENE EL USUARIO
	 */
	private static int contabilizarFallos(String caracteresElegidos, int fallos) {
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}
	
	/**
	 * ESTE METODO SE ENCARGA DE MOSTRAR LA PALABRA QUE ESTA OCULTA.
	 * CADA LETRA QUE EL USUARIO VAYA ACERTANDO SER� REVELADA.
	 */
	private static void palabraOculta() {
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}
	
	/**
	 * LOS SIGUIENTES SIGUIENTES METODOS CONTIENEN LOS
	 * SYSO QUE UTILIZAMOS EN EL SWITCH PARA LOS FALLOS.
	 */

	private static void case7() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println(" O    |");
		System.out.println(" T    |");
		System.out.println(" A   ___");
	}

	private static void case6() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println(" O    |");
		System.out.println(" T    |");
		System.out.println("     ___");

	}

	private static void case5() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println(" O    |");
		System.out.println("      |");
		System.out.println("     ___");

	}

	private static void case4() {
		System.out.println("  ____ ");
		System.out.println(" |    |");
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("     ___");

	}

	private static void case3() {
		System.out.println("  ____ ");
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("     ___");
	}

	private static void case2() {
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("      |");
		System.out.println("     ___");

	}

	private static void case1() {
		System.out.println("     ___");
	}
	
	/**
	 * ESTE METODO RELLENA EL VECTOR PALABRAS CON LAS CADENAS
	 * QUE EL USUARIO TENDRA QUE AVERIGUAR
	 */
	private static void rellenarVectorPalabras() {
		String ruta = "src\\palabras.txt"; //Variables en lineas diferentes
		File fich = new File(ruta);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

}