/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea de de bloque.
 */


/**
 * @author fvaldeon
 * @since 31-01-2018
 */
 
import java.util.Scanner;
//Se deben evitar los import innecesarios.
// Sentencias 'import' cambiadas de encima a debajo de los comentarios de la clase.
public class Refactorizacion1 {
	//Nombre de la clase en UpperCamelCase
	final static String SALUDO = "Bienvenido al programa";
	//Nombre más descriptivo para la constante escrito en mayúsculas.
	public static void main(String[] args) {
		String cadena1;
		String cadena2; //Declarar una única variable por línea.
		Scanner input = new Scanner(System.in); //Declarar las variables justo antes de su uso.
		//Nombre más descriptivo para la variable
		System.out.println(SALUDO);
		System.out.println("Introduce tu dni");
		cadena1 = input.nextLine();
		System.out.println("Introduce tu nombre");
		cadena2 = input.nextLine();
		
		int numeroA = 7;
		//Cambio de nombre por uno más descriptivo
		int numeroB = 16;	
		//Cambio de nombre por uno más descriptivo
		//Se tiene que indicar una variable por línea e inicializarlas después de declararlas,
		//antes de su uso para reducir su ámbito.
		int numeroC = 25;
		//Cambio de nombre por uno más descriptivo
		//Cambio al principio de la clase.
		if(numeroA > numeroB || (numeroC % 5) != 0 && (numeroC * 3) - 1 > (numeroB / numeroC)) {
			//Paréntesis para que las operaciones sean más legibles.
			System.out.println("Se cumple la condición");
		}
		
		numeroC = numeroA + (numeroB * numeroC) + (numeroB / numeroA);
		//Paréntesis para que las operaciones sean más legibles.
		
		String[] diasSemana={"Lunes", "Martes", "Miércoles",
			"Jueves", "Viernes", "Sábado", "Domingo"};
		//Partir la línea cuando excede de 100 caractéres.
		//Cambio de nombre por uno más descriptivo.
		//Declarado justo antes de su uso.
		//Los corchetes deben ir indicados justo después del tipo de dato.
		//Además los arrays se pueden inicialiazar en bloque.
		visualizarDiasSemana(diasSemana);
	}
	static void visualizarDiasSemana(String[] diasSemana) {
	//Cambio de nombre por uno más descriptivo.
	//Los corchetes de un array van unidos al tipo de dato.
	//Los nombres de métodos y variables deben estar escritos en lowerCamelCase.	
		for(int i = 0; i < diasSemana.length; i++) {
			//Sustituir magic numbers por constantes que identifican su finalidad
			//Cambio del nombre del contador, empiezan siempre en i.
			System.out.println("El dia de la semana en el que te encuentras ["+(i+1)+"-7] es el"
				+"+dia: "+diasSemana[i]);			
			//Partir la línea cuando excede de 100 caractéres
		}
	}
	
}