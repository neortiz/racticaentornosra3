package principal;

import java.util.Scanner;

/** 
 * CLASE QUE GENERA CONTRASE�AS ALEATORIAS
 * 
 * @author NEREA ORTIZ PE�ALOSA
 * @since 21-2-2018
 * @version 1.2
 */
public class Generador {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		elegirOpcionDeContrase�a();
		
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		
		String password = switchGenerarContrase�a(longitud, opcion);

		System.out.println(password);
		scanner.close();
	}
	
	/**
	 * ESTE METODO CONTIENE EL SWITCH, QUE GENERAR�
	 * UNA CONTRASE�A ALEATORIA.
	 * 
	 * @param longitud : LONGITUD DE LA CONTRASE�A
	 * @param opcion : TIPO DE CONTRASE�A ELEGIDA
	 * @return password : LA CONTRASE�A GENERADA
	 */
	public static String switchGenerarContrase�a(int longitud, int opcion) {
		String password = "";
		for (int i = 0; i < longitud; i++) {
			switch (opcion) {
				case 1:
					password += generarLetraAleatoria();					
					break;
				case 2:
					password += generarNumeroAleatorio();
					break;
				case 3:
					password += generarCaracterEspecialOLetra();
					break;
				case 4:
					password += generarContrase�aTipoCuatro();
					break;
			}
		}
		return password;
	}
	
	/**
	 * ESTE M�TODO GENERA UN N�MERO, LETRA O CAR�CTER ESPECIAL
	 * DE FORMA ALEATORIA.
	 * 
	 * @return DEVUELVE UN CHAR QUE CORRESPONDE A UN NUMERO,
	 * LETRA O CARACTER ESPECIAL GENERADO ALEATORIAMENTE
	 */
	public static char generarContrase�aTipoCuatro() {
		byte n = (byte) (Math.random() * 2);
		if(n == 1) return generarNumeroAleatorio();
		else return generarCaracterEspecialOLetra();
	}
	
	/**
	 * ESTE M�TODO MUESTRA AL USUARIO LAS DISTINTAS
	 * OPCIONES QUE PUEDE SELECCIONAR PARA CREAR
	 * UNA CONTRASE�A
	 */
	public static void elegirOpcionDeContrase�a() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}
	
	/**
	 * ESTE M�TODO GENERA UNA LETRA O CAR�CTER
	 * ESPECIAL DE FORMA ALEATORIA.
	 * 
	 * @return DEVUELVE UN CHAR QUE CORRESPONDE A UNA LETRA,
	 * O CAR�CTER ESPECIAL GENERADO ALEATORIAMENTE
	 */
	public static char generarCaracterEspecialOLetra(){
		byte n = (byte) (Math.random() * 2);
		if(n==0) return generarLetraAleatoria();
		else return generarCaracterEspecial();
	}
	
	/**
	 * ESTE M�TODO GENERA UN N�MERO ALEATORIO
	 * 
	 * @return DEVUELVE UN N�MERO COMO CHAR
	 */
	public static char generarNumeroAleatorio() {
		return (char) ((Math.random() * 10) + 48);
	}
	
	/**
	 * ESTE M�TODO GENERA UNA LETRA ALEATORIA
	 * 
	 * @return DEVUELVE UNA LETRA COMO CHAR
	 */
	public static char generarLetraAleatoria() {
		return (char) ((Math.random() * 26) + 65);
	}
	
	/**
	 * ESTE M�TODO GENERA UN CAR�CTER ESPECIAL
	 * ALEATORIO
	 * 
	 * @return DEVUELVE UN CAR�CTER ESPECIAL COMO
	 * CHAR
	 */
	public static char generarCaracterEspecial() {
		return (char) ((Math.random() * 15) + 33);
	}

}
