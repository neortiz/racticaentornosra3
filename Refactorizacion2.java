/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.util.Scanner;
//Importar solo las clases necesarias
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizacion2 {
//Cambiar el nombre de la clase para que coincida con el nombre del archivo
	static Scanner input;//Cambiar de nombre por uno más descriptivo
	public static void main(String[] args) 
	{
	
		input = new Scanner(System.in);
		
		int cantidadMaximaAlumnos = 10;//Se declara una variable por línea
		//El nombre de las variables deben estar en lowerCamelCase
		//Las variables se inicalizan al momento de declararse
		int[] notasAlumnos = new int[cantidadMaximaAlumnos];//Los corchetes se colocan junto al tipo de dato
		//Cambiar el nombre por uno más descriptivo
		for(int i=0;i<cantidadMaximaAlumnos;i++)//Las variables se tienen que declarar justo antes de su uso.
		//Los contadores se llaman con una letra comenzando por la i
		{
			System.out.println("Introduce nota media de alumno");
			notasAlumnos[i] = input.nextLine();
		}	
		
		System.out.println("El resultado es: " + notaMediaClase(notasAlumnos);
		
		input.close();
	}
	static double notaMediaClase(int[] vector)//Cambiar de nombre por uno más descriptivo
	//El nombre de los métodos deben estar en lowerCamelCase
	//Los corchetes se colocan junto al tipo de dato
	{
		double suma = 0;//Cambiar de nombre por uno más descriptivo
		
		for(int j=0;j<10;j++)} //Los contadores se llaman con una letra comenzando por la i
		//Las llaves se abren justo al lado de la condición
			suma += vector[j];//Queda mejor +=
		}
		return suma/10;
	}
	
}